<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>24HOUR ─ Home</title>
</head>
<body>
    <div class="container">
        <div class="topbar">
            <a href="/dashboard" class="topbarlink">ACCOUNT<svg class="svg1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
            </svg></a> 
        </div>
    <header>
        <a href="/"><img src="imgs/logo.png" alt="" class="logo"></a>
    </header>
    <br><br>
    <nav>
        <ul>
            <li><a href="/about" class="navlinks">ABOUT</a></li>
            <li><a href="/faq" class="navlinks">FAQ</a></li>
            <li><a href="/memberships" class="navlinks" id="memberships">MEMBERSHIPS</a></li>
            <li><a href="/facilities" class="navlinks">FACILITIES</a></li>
            <li><a href="/contact" class="navlinks">CONTACT US</a></li>
        </ul>
    </nav>
    </div>
    <div class="div1">
        <a href="#socials" class="div1link">MAKE SURE TO FOLLOW US ON OUR SOCIALS</a>
        </a>
    </div>
    <img src="imgs/image1.png" alt="" class="image1">
    <div class="div2">
        <h1 class="h1thick">UP TO 50% OFF*</h1>
        <h3 class="h3sale">SALE ON NOW</h3>
    </div>
    <br><br>
    <h1 class="h1team">OUR TEAM</h1>
    <div class="team">
        <div class="teamdiv">
            <img src="imgs/team1.png" alt="">
            <p class="name">Emily Hawk</p>
            <p class="position">Head of Operations & Trainer</p>
            <button class="btn" onclick="location.href='/emily-hawks'">VIEW MORE</button>
        </div>
        <div class="teamdiv">
            <img src="imgs/team2.png" alt="">
            <p class="name">Lynette Banks</p>
            <p class="position" id="fitnessleader">Group Fitness Leader</p>
            <button class="btn" onclick="location.href='/lynette-banks'">VIEW MORE</button>
        </div>
        <div class="teamdiv">
            <img src="imgs/team3.png" alt="">
            <p class="name">Ryan Hiles</p>
            <p class="position">Head of Operations & Trainer</p>
            <button class="btn" onclick="location.href='/ryan-hiles'">VIEW MORE</button>
        </div>
    </div>
    <br>
    <video autoplay loop muted plays-inline class="workoutvid">
        <source src="vids/workout.mp4" type="video/mp4">
    </video>
    <br>
    <div class="div3">
        <p class="title1">THE BEST GYM IN GEORGIA</p>
        <p class="titletext1">At 24HOUR, our mission is to provide our gym members with more ways to reach their fitness goals, more advice<br> 
            for a healthy lifestyle and more opportunities to feel unstoppable. We know getting started is tough, so we have<br> 
            all the tools to make workouts fun and energizing. We have personal training programs to jumpstart your workout<br> schedule and nutritional advice to get you eating right. Add to that, group exercise classes, cardio theaters and<br> 
            swimming pools, and you know you’re in the right place to transform your life and your confidence.</p>
    </div>
    <br>
    <div class="div4">
        <h3><h3>KEEP AN EYE ON EMAIL UPDATES AND GET A CHANCE TO WIN 50% OFF* A CERTAIN MEMBERSHIP.</h3></h3>
    </div>
    <footer>
        <div class="customerservice">
            <p class="title2">CUSTOMER SERVICE</p>
            <p><a href="/account/registration" class="footerlinks">Register a New Account</a><p>
            <p><a href="/contact" class="footerlinks">Contact Us</a></p>
            <p><a href="/faq" class="footerlinks">FAQs</a></p>
        </div>
        <div class="myaccount">
            <p class="title2">MY ACCOUNT</p>
            <p><a href="../account/login" class="footerlinks">Sign In to My Account</a></p>
        </div>
        <div class="policies">
            <p class="title2">POLICIES</p>
            <p><a href="/privacy" class="footerlinks">Privacy & Cookie Policy</a></p>
            <p><a href="/terms" class="footerlinks">Terms & Conditions</a></p>
            <a href="/review" class="footerlinks">Review Guidelines</a>
        </div>
        <div class="followus">
            <p class="title2">FOLLOW US</p>
            <div class="socials" id="socials">
                <a href="http://www.facebook.com"><img src="imgs/facebook.png" alt=""></a>
                <a href="http://www.instagram.com"><img src="imgs/instagram.png" alt=""></a>
                <a href="http://www.twitter.com"><img src="imgs/twitter.png" alt="" id="twitter"></a>
            </div>
        </div>
    </footer>
    <div class="div5">
        <img src="imgs/logo.png" alt="" class="div5logo">
        <img src="imgs/links.png" alt="" class="payments">
    </div>
</body>
</html>
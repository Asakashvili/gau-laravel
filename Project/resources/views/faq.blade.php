<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="alternate" href="atom.xml" type="application/atom+xml" title="Atom">
    <link rel="stylesheet" href="css/style.css">
    <title>24HOUR ─ FAQ</title>
</head>
<body>
    <div class="container">
        <div class="topbar">
            <a href="/dashboard" class="topbarlink">ACCOUNT<svg class="svg1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
            </svg></a> 
        </div>
    <header>
        <a href="/"><img src="imgs/logo.png" alt="" class="logo"></a>
    </header>
    <br><br>
    <nav>
        <ul>
            <li><a href="/about" class="navlinks">ABOUT</a></li>
            <li><a href="/faq" class="navlinks">FAQ</a></li>
            <li><a href="/memberships" class="navlinks" id="memberships">MEMBERSHIPS</a></li>
            <li><a href="/facilities" class="navlinks">FACILITIES</a></li>
            <li><a href="/contact" class="navlinks">CONTACT US</a></li>
        </ul>
    </nav>
    </div>
    <div class="div1">
        <a href="#socials" class="div1link">MAKE SURE TO FOLLOW US ON OUR SOCIALS</a>
    </div>
    <div class="left">
        <h1 class="thick">FREQUENTLY ASKED QUESTIONS</h1>
        <div class="pagelinks">
            <a href="/" class="link1">HOME</a>
            <span class="slash">/</span>
            <a href="/faq" class="link2">FAQ</a>
        </div>
        <br>

        <div class="accordion">
            <div class="accordion-item">
              <div class="accordion-item-header">WHAT TIME DOES THE GYM OPEN?</div>
              <div class="accordion-item-body">
                <div class="accordion-item-body-content">
                    <p>
                        The gym is working
                        <b></b>
                        8:00 - 23:45
                        <br><br>
                        User registration is possible (subscription purchase)
                        <br>
                        from 8:00 to 22:00
                    </p>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <div class="accordion-item-header">WHAT IS AN INDIVIDUAL PROGRAM?</div>
              <div class="accordion-item-body">
                <div class="accordion-item-body-content">
                    <p>
                        An individual program is an individualized exercise and nutrition program that helps a person to achieve a set goal (weight loss, weight maintenance, mass gain, body contouring). Preparation of the program, an individual program is prepared by professional coaches, with 3 lessons during which the program is prepared.
                        <br><br>
                        Price is 50GEL
                    </p>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <div class="accordion-item-header">WHAT IS A PERSONAL FITNESS ASSISTANT SERVICE AND WHAT DOES IT INCLUDE?</div>
              <div class="accordion-item-body">
                <div class="accordion-item-body-content">
                    <p>
                        The service of a personal fitness assistant means the service of a semi-remote personal trainer - the cost of 1 month is 150GEL
                        <br><br><br>
                        What you get from the assistant:
                        <br><br>
                        - The assistant will study your current fitness and goals, based on which he will prepare an exercise and nutrition program tailored to you based on your goals - weight loss, weight maintenance, weight gain, figure out the shapes, drawing up an exercise program (3 or 4 days), it takes place in the gym, the coach sets it up and explains it to you How to perform which exercise<br>
                        - Sets short and long-term goals (in terms of the program)<br>
                        - Will monitor the performance of the set goals (weekly / remotely)<br>
                        - If necessary, will adjust the program (depending on the dynamics and weekly results - remotely or on the spot, depending on the need)<br>
                        - Will be in constant contact with you - Will do weekly and monthly evaluations<br>
                        - High level of involvement will help you achieve your goals<br>
                    </p>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <div class="accordion-item-header">WHAT ARE THE WORKING HOURS OF THE COACHES?</div>
              <div class="accordion-item-body">
                <div class="accordion-item-body-content">
                    <p>
                        Coaches work every day
                        <br>
                        Monday/Tuesday/Wednesday/Thursday/Friday - 9:00 a.m. to 11:00 p.m.
                        <br>
                        Saturday/Sunday - from 9:00 to 22:00
                    </p>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <div class="accordion-item-header">WHAT IS INCLUDED IN THE STANDARD SUBSCRIPTION?</div>
              <div class="accordion-item-body">
                <div class="accordion-item-body-content">
                    <p>
                        - Consultation of coaches and training under their supervision<br>
                        - Fitness - Simulators and free weights<br>
                        - Cardio zone - Treadmills, skis and bicycles<br>
                        - Aerobics - Aerobics zone<br>
                        - Additional - Changing rooms and showers, Finnish sauna<br>
                        - Fitness bar - Food supplements and drinks<br>
                    </p>
                </div>
              </div>
            </div>
          </div>
    </div>
    <br>
    <div class="div4">
        <h3><h3>KEEP AN EYE ON EMAIL UPDATES AND GET A CHANCE TO WIN 50% OFF* A CERTAIN MEMBERSHIP.</h3></h3>
    </div>
    <footer>
        <div class="customerservice">
            <p class="title2">CUSTOMER SERVICE</p>
            <p><a href="/account/registration" class="footerlinks">Register a New Account</a><p>
            <p><a href="/contact" class="footerlinks">Contact Us</a></p>
            <p><a href="/faq" class="footerlinks">FAQs</a></p>
        </div>
        <div class="myaccount">
            <p class="title2">MY ACCOUNT</p>
            <p><a href="/account/login" class="footerlinks">Sign In to My Account</a></p>
        </div>
        <div class="policies">
            <p class="title2">POLICIES</p>
            <p><a href="/privacy" class="footerlinks">Privacy & Cookie Policy</a></p>
            <p><a href="/terms" class="footerlinks">Terms & Conditions</a></p>
            <a href="/review" class="footerlinks">Review Guidelines</a>
        </div>
        <div class="followus">
            <p class="title2">FOLLOW US</p>
            <div class="socials" id="socials">
                <a href="http://www.facebook.com"><img src="imgs/facebook.png" alt=""></a>
                <a href="http://www.instagram.com"><img src="imgs/instagram.png" alt=""></a>
                <a href="http://www.twitter.com"><img src="imgs/twitter.png" alt="" id="twitter"></a>
            </div>
        </div>
    </footer>
    <div class="div5">
        <img src="imgs/logo.png" alt="" class="div5logo">
        <img src="imgs/links.png" alt="" class="payments">
    </div>
    <script src="js/script.js"></script>
</body>
</html>
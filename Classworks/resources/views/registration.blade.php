<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <h1>Registration</h1>
    <form action="/signup" method="post">
        @csrf
        <input type="text" name="first_name"> - firstname
        <br><br>
        <input type="text" name="last_name"> - lastname
        <br><br>
        <input type="text" name="email"> - email
        <br><br>
        <input type="text" name="id"> - ID
        <br><br>
        <button>Register</button>
        <script src="../js/my-script.js"></script>
    </form>
</body>
</html>
<?php

// 'namespace' შეიძლება განისაზღვროს, როგორც ელემენტების კლასი, რომელშიც თითოეულ ელემენტს აქვს უნიკალური სახელი.
namespace App\Http\Controllers;

// აწვდის ვალიდაციის მეთოდებს.

use App\Models\Form;
use Illuminate\Http\Request; 

// ეს ლაინი დაგენერირდა ავტომატურად, ეს ფაილი არის 'Controller' ფაილის შვილობილი.
class Validation extends Controller
{
    //

    public function index(){
        return view ('form');
    }

    // შევქმენი ფუნქცია, დავარქვი სახელი და ჩავუწერე 'request' - მოთხოვნის პარამეტრი.
    public function store(request $request)
    {
        $request->validate([ // განვსაზღვრე '$request' ცვლადს მივწვდი 'validate()' მეთოდს.

            // 'validate()' მეთოდში ისრის მარცხნივ ჩავწერე 'input'-ის სახელები ხოლო მარჯვნივ ვალიდაციის წესები.
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:64|max:128',
            'email' => 'required|email',
            'password' => 'required|password',
            'repeatpassword' => 'required|same:password,'

            // თუ მუშაობს კოდის ჩატვირთვა დასრულდება წარმატებით.
            // თუ არ მუშაობს, გამოვა 'ValidationException'.
        ]);
    return $request->input();
    }
}









<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// პირველი სტრინგი წარმოადგენს ლინკს.

// მეორე სტრინგი წარმოადგენს თუ ამ კონკრეტულ ლინკზე რომელი '.blade.php' გაფართოების ფაილი ჩაიტვირთოს.
// ფაილებს ვუთითებთ გაფართოების გარეშე.

// GET - რესურსის მოთხოვნა.
// POST - რესურსის შექმნა.

Route::get('/', function () {
    return view('home');
});

Route::get('/registration', function () {
    return view('registration');
});

Route::get('/registration/form', function () {
    return view('form');
});

Route::post('validation','Validation@index');






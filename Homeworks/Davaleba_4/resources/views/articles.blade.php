<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>სტატიები</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <header><a class="title">FLOWERS SHOP</a></header>
    <div class="div1"><span class="ha">LABORATORY<br>IV</span></div>
    <nav>
        <ul>
            <li><a href="/">მთავარი</a></li>
            <li><a href="/about">ჩვენს შესახებ</a></li>
            <li><a href="/categories">კატეგორიები</a></li>
            <li><a href="/articles">სტატიები</a></li>
            <li><a href="/contact">კონტაქტი</a></li>
        </ul>
    </nav>
    <br><br>
    <div class="div2">
        <h2>ვარდის დარგვა — დასარგავი ადგილის შერჩევა, მომზადება, დარგვა</h2>
        <p><i>ვარდებისთვის ადგილის შერჩევა ორი მოსაზრებით უნდა მოხდეს: ეკოლოგიური (ვინაიდან ვარდი სინათლისა და სითბოს 
        მოყვარული მცენარეა, ყველა აუცილებელი პირობა უნდა იქნას გათვალისწინებული მცენარეთა ზრდა-განვითარებისათვის) 
        და დეკორატიული (ადგილმდებარეობის შერჩევა განსაზღვრავს მის მომავალ სილამაზეს. ჰარმონიული...</i></p>
        <button onClick="location.href='/article1'" class="btn1">წაკითხვა</button>
        <br><br>
        <h2>როგორ მოვრწყათ ყვავილები</h2>
        <p><i>მეყვავილეთა უმრავლესობა სარწყავში ჩვეულებრივ, ონკანის წყალს ასხამს და საჭიროების შემთხვევაში მასში სასუქს ამატებს…
        გამძლე მცენარეებისთვის ასეთი მოპყრობა სრულიად მისაღებია, თუმცა, შედარებით მგრძნობიარე მცენარეებს განსაკუთრებული 
        მოთხოვნები აქვთ. პირველ რიგში, ყურადღება უნდა მიაქციოთ წყლის ტემპერატუ...</i></p>
        <button onClick="location.href='/article2'" class="btn1">წაკითხვა</button>
        <br><br>
        <h2>როგორ მოვუაროთ Dracaena Compacta_ს</h2>
        <p><i>დრაცენა კომპაკტა არის დრაცენას ოჯახის ერთ-ერთი სახეობა, რომლის სამშობლო სამხრეთ აფრიკაა. ის ხშირად ასოცირდება დრაცენა ფრაგრანს და დრაცენა დერემენსისის ჯიშებთან. დრაცენა კომპაკტას აქვს სქელი, მწვანე ღეროები. ის არაჩეულებრივი დეკორატიული მცენარეა სახლის თუ ოფისის ინტერიერისთვის. სინათლე დრაცენა კო...</i></p>
        <button onClick="location.href='/article3'" class="btn1">წაკითხვა</button>
        <br><br><br>
    </div>
</body>
</html>
<?php

namespace App\Http\Controllers;

use App\Models\Contact; // დავაკავშირე მოდელი.

use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function contact(){
        return view("contact");    
    }

    // VALIDATIONS
    public function submitContact(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:contacts',
            'mobile'=>'required|min:6',
            // 'message'=>'required',
        ]);

        // SAVING DATA INTO THE DATABASE
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->mobile = $request->mobile;
        $contact->message = $request->message;
        $result = $contact->save();
        if($result){ // IF TRUE
            return back()->with('success', 'You have successfully registered');
        }else{ // IF NOT TRUE
            return back()->with('fail', 'Something is wrong!');
        }
    }
}

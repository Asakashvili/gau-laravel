<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <header></header>
    <nav>
        <ul>
            <li><a href="/" class="alink">Home</a></li>
            <li><a href="/registration" class="alink">Registration</a></li>
        </ul>
    </nav>

    <form action="/registration/form" method="POST">
        @csrf
        <h2>Registration</h2>
        <br>
        <label for="firstname" class="label1">First Name:</label>
        <br>
        <input type="text" name="firstname" class="inp" placeholder="Enter your First Name here...">
        <br><br>

        <label for="lastname" class="label2">Last Name:</label>
        <br>
        <input type="text" name="lastname" class="inp" placeholder="Enter your Last Name here...">
        <br><br>

        <label for="mobile" class="label3">Phone Number:</label>
        <br>
        <input type="text" name="mobile" class="inp" placeholder="haha" value="(+995) ">
        <br><br>

        <label for="gender" class="label4">Gender:</label>
        <br>
        <div class="div1">
        <input type="radio" name="gender" value="Male">Male
        <input type="radio" name="gender" value="Female">Female
        <input type="radio" name="gender" value="Neither">Neither
        <br><br>
        </div>

        <label for="email" class="label5">Email:</label>
        <br>
        <input type="email" name="email" class="inp" placeholder="Enter your E-Mail address here...">
        <br><br>

        <label for="password" class="label6">Password:</label>
        <br>
        <input type="password" name="password" class="inp" placeholder="Enter your Password here...">
        <br><br>

        <label for="repeatpassword" class="label7">Repeat Password:</label>
        <br>
        <input type="password" name="repeatpassword" class="inp" placeholder="Repeat your Password here...">
        <br><br><br>

        <button class="btn">REGISTER</button>
        <br><br><br><br><br>
    </form>
</body>
</html>
<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController; // <- 'web.php' ფაილს მივაბი ჩემს მიერ შექმნილი კონტროლერი.
use App\Http\Controllers\UserController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/facilities', function () {
    return view('facilities');
});

Route::get('/memberships', function () {
    return view('memberships');
});

Route::get('/panel', function () {
    return view('panel');
});

Route::get('/emily-hawks', function () {
    return view('emily-hawks');
});

Route::get('/lynette-banks', function () {
    return view('lynette-banks');
});

Route::get('/ryan-hiles', function () {
    return view('ryan-hiles');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/terms', function () {
    return view('terms');
});

Route::get('/review', function () {
    return view('review');
});



// აქ ლინკებს მივაბი კონტროლერი თავისი ფუნქციის სახელებით.
// 'GET' is for showing pags and 'POST' is for storing data.
route::get('/account/login',[CustomAuthController::class, 'login'])->middleware('alreadyLoggedIn');
route::get('/account/registration',[CustomAuthController::class, 'registration']);
Route::post('/register-user',[CustomAuthController::class, 'registerUser'])->name('register-user');
Route::post('login-user',[CustomAuthController::class, 'loginUser'])->name('login-user');
Route::get('/dashboard',[CustomAuthController::class, 'dashboard'])->middleware('isLoggedIn');
Route::get('/logout',[CustomAuthController::class, 'logout']);
Route::get('/panel',[CustomAuthController::class, 'panel'])->middleware('isLoggedIn');

// DISPLAY
Route::get('/panel',[UserController::class, 'show'])->middleware('isLoggedIn');

// DELETE
Route::get('/click_delete/{id}',[UserController::class, 'delete']); // GOES TO DELETE FUNCTION

// CONTACTS
route::get('/contact',[ContactController::class, 'contact']);
Route::post('submit-contact',[ContactController::class, 'submitContact'])->name('submit-contact');


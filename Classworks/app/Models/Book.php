<?php
// მოდელის სახელებს ვწერთ მხოლობითში, თუ ცხრილის სახელია 'Users' აქ ვარქმევთ 'User'.
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
}

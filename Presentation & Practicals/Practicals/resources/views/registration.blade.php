<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Page</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <header></header>
    <nav>
        <ul>
            <li><a href="/" class="alink">Home</a></li>
            <li><a href="/registration" class="alink">Registration</a></li>
        </ul>
    </nav>
    <h2 class="b2">..and this is a Registration page!</h2>
    <p class="p1">Would you like to <a href="/registration/form" class="alink2"><b>Register?</b></a></p>
</body>
</html>
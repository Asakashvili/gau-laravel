<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>კატეგორიები - თაიგულები</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <header><a class="title">FLOWERS SHOP</a></header>
    <div class="div1"><span class="ha">LABORATORY<br>IV</span></div>
    <nav>
        <ul>
            <li><a href="/">მთავარი</a></li>
            <li><a href="/about">ჩვენს შესახებ</a></li>
            <li><a href="/categories">კატეგორიები</a></li>
            <li><a href="/articles">სტატიები</a></li>
            <li><a href="/contact">კონტაქტი</a></li>
        </ul>
    </nav>
    <br><br>
    <div class="container">
        <a href="/bought" class="flowera"><div class="flower1">25 ვარდი ლარნაკში<span class="price1">260ლ</span></div></a>&nbsp&nbsp&nbsp&nbsp
        <a href="/bought" class="flowera"><div class="flower2">36 წითელი ვარდი<span class="price2">300ლ</span></div></a>&nbsp&nbsp&nbsp&nbsp
        <a href="/bought" class="flowera"><div class="flower3">ვარდების თაიგული<span class="price3">130ლ</span></div></a>&nbsp&nbsp&nbsp&nbsp
    </div>
    <br><br><br><br>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>კატეგორიები - თაიგულები</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <header><a class="title">FLOWERS SHOP</a></header>
    <div class="div1"><span class="ha">LABORATORY<br>IV</span></div>
    <nav>
        <ul>
            <li><a href="/">მთავარი</a></li>
            <li><a href="/about">ჩვენს შესახებ</a></li>
            <li><a href="/categories">კატეგორიები</a></li>
            <li><a href="/articles">სტატიები</a></li>
            <li><a href="/contact">კონტაქტი</a></li>
        </ul>
    </nav>
    <br><br>
    <h3>გილოცავთ, შეძენა დასრულდა წარმატებით!</h3>
    <br><br><br>
</body>
</html>

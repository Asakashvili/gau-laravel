@extends('base')

@section('sidebar')
    @parent
    <h2>Book Create Blade.</h2>
@endsection

@section('content')
    <form action="/book/store" method="post">
        @csrf
        <input type="text" name="title" valie="{{$title}}"> - title
        <br><br>
        <input type="text" name="author"> - author
        <br><br>
        <input type="date" name="date"> - date
        <br><br>
        <input type="number" name="amount"> - amount
        <br><br>
        <input type="number" name="price"> - price
        <br><br>
        <button>INSERT</button>
    </form>
@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User; // დავაკავშირე მოდელი.
use Illuminate\Support\Facades\Hash; // პაროლის დამალვა.
use Illuminate\Support\Facades\Session; // სესიის შემოტანა.

class CustomAuthController extends Controller
{
    // LOGIN & SIGNUP FUNCTIONS\
    public function login(){
        return  view("auth.login");
    } 

    public function registration(){
        return view("auth.registration");    
    }

    // VALIDATIONS
    public function registerUser(Request $request){
        $request->validate([
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required|email|unique:users', // აქ იმეილს დავუწერე უნიკალურობა და მივაბი მონაცემთა ბაზის სახელი.
            'password'=>'required|min:6|max:12',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);

        // SAVING DATA INTO THE DATABASE
        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $result = $user->save();
        if($result){ // IF TRUE
            return back()->with('success', 'You have successfully registered');
        }else{ // IF NOT TRUE
            return back()->with('fail', 'Something is wrong!');
        }
    }

    // LOGIN VALIDATIONS
    public function loginUser(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|min:6|max:12',
        ]);

        // GETTING REQUEST FOR EMAIL AND PASSWORD
        $user = User::where('email', '=', $request->email)->first(); // EMAIL EQUALS TO THE REQUESTED EMAIL
        if($user){ // IF TRUE
            if(Hash::check($request->password, $user->password)){ // CHECKING THE ENCRYPTED PASSWORD / IF SUCCESSFUL -> LOGIN
                $request->session()->put('loginId', $user->id); // SAVING THE ID OF THE LOGGED IN USER
                return redirect('dashboard'); // REDIRECTING STRAIGHT TO THE DASHBOARD
            }else{ // IF NOT SUCCESSFULL
                return back()->with('fail', 'Password not matches!');
            }
        }else{ // IF NOT TRUE
            return back()->with('fail', 'This email is not registered!');
        }
    }

    // DASHBOARD
    public function dashboard(){
        $data = array();
        if(Session::has('loginId')){
            $data = User::where('id', '=', Session::get('loginId'))->first();
        }
        return view('dashboard', compact('data')); // PASSING THE DATA INTO THE BLADE FILE
    }

    // LOGOUT METHOD
    public function logout(){
        if(Session::has('loginId')){ // IF EXISTS THEN PULL
            Session::pull('loginId');
            return redirect('/account/login');
        }
    }

    public function panel(){
        $data = array();
        if(Session::has('loginId')){
            $data = User::where('id', '=', Session::get('loginId'))->first();
        }
        return view('panel', compact('data')); // PASSING THE DATA INTO THE BLADE FILE
    }
}

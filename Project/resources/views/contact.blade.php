<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="alternate" href="atom.xml" type="application/atom+xml" title="Atom">
    <link rel="stylesheet" href="css/style.css">
    <title>24HOUR ─ Contact Us</title>
</head>
<body>
    <div class="container">
        <div class="topbar">
            <a href="/dashboard" class="topbarlink">ACCOUNT<svg class="svg1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
            </svg></a> 
        </div>
    <header>
        <a href="/"><img src="imgs/logo.png" alt="" class="logo"></a>
    </header>
    <br><br>
    <nav>
        <ul>
            <li><a href="/about" class="navlinks">ABOUT</a></li>
            <li><a href="/faq" class="navlinks">FAQ</a></li>
            <li><a href="/memberships" class="navlinks" id="memberships">MEMBERSHIPS</a></li>
            <li><a href="/facilities" class="navlinks">FACILITIES</a></li>
            <li><a href="/contact" class="navlinks">CONTACT US</a></li>
        </ul>
    </nav>
    </div>
    <div class="div1">
        <a href="#socials" class="div1link">MAKE SURE TO FOLLOW US ON OUR SOCIALS</a>
    </div>
    <div class="left">
        <h1 class="thick">CONTACT US</h1>
        <div class="pagelinks">
            <a href="/" class="link1">HOME</a>
            <span class="slash">/</span>
            <a href="/contact" class="link2">CONTACT US</a>
        </div>
    </div>
    <br>
    <p class="contactinfo1">
        We are committed to answering your questions and addressing comments<br> 
        in a timely fashion. For the quickest reply, please complete the form below.
    </p>
    <div class="infoinp">
        <form action="{{route('submit-contact')}}" method="post">
            @csrf
            <input type="text" class="inp" placeholder="Name *" name="name" value="{{old('name')}}">
            <span class="text-danger">@error('name') {{$message}} @enderror</span>
            <br><br>
            <input type="text" class="inp" placeholder="Email *" name="email" value="{{old('email')}}">
            <span class="text-danger">@error('email') {{$message}} @enderror</span>
            <br><br>
            <input type="number" class="inp" placeholder="Mobile *" name="mobile" value="{{old('mobile')}}">
            <span class="text-danger">@error('mobile') {{$message}} @enderror</span>
            <br><br>
            <textarea name="textarea" cols="80" rows="5" placeholder="Message *" name="message" value="{{old('message')}}"></textarea>
            <span class="text-danger">@error('message') {{$message}} @enderror</span>
            <br><br>
            <button class="btn-contact" type="submit">SUBMIT</button>
        </form>
        <p class="contactinfo1">
            Alternatively, please email us if you have a question, sales enquiry or you<br>
            would like to give us your feedback.
            <br><br><br>
            <span class="mail">Email us <a>24hourgym@gmail.com</a></span>
        </p>
    </div>
    <br>
    <div class="div4">
        <h3><h3>KEEP AN EYE ON EMAIL UPDATES AND GET A CHANCE TO WIN 50% OFF* A CERTAIN MEMBERSHIP.</h3></h3>
    </div>
    <footer>
        <div class="customerservice">
            <p class="title2">CUSTOMER SERVICE</p>
            <p><a href="/account/registration" class="footerlinks">Register a New Account</a><p>
            <p><a href="/contact" class="footerlinks">Contact Us</a></p>
            <p><a href="/faq" class="footerlinks">FAQs</a></p>
        </div>
        <div class="myaccount">
            <p class="title2">MY ACCOUNT</p>
            <p><a href="/account/login" class="footerlinks">Sign In to My Account</a></p>
        </div>
        <div class="policies">
            <p class="title2">POLICIES</p>
            <p><a href="/privacy" class="footerlinks">Privacy & Cookie Policy</a></p>
            <p><a href="/terms" class="footerlinks">Terms & Conditions</a></p>
            <a href="/review" class="footerlinks">Review Guidelines</a>
        </div>
        <div class="followus">
            <p class="title2">FOLLOW US</p>
            <div class="socials" id="socials">
                <a href="http://www.facebook.com"><img src="imgs/facebook.png" alt=""></a>
                <a href="http://www.instagram.com"><img src="imgs/instagram.png" alt=""></a>
                <a href="http://www.twitter.com"><img src="imgs/twitter.png" alt="" id="twitter"></a>
            </div>
        </div>
    </footer>
    <div class="div5">
        <img src="imgs/logo.png" alt="" class="div5logo">
        <img src="imgs/links.png" alt="" class="payments">
    </div>
</body>
</html>

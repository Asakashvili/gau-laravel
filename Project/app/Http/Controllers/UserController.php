<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\User;

class UserController extends Controller
{
    // LIST
    public function show(){
       $data = User::all();
        return view('panel', ['users'=>$data]);
    }

    // DELETE
    public function delete($id){
        DB::delete('delete from users where id = ?', [$id]);
        return redirect('panel')->with('success', 'Data Deleted');
    }
}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>კონტაქტი</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <header><a class="title">FLOWERS SHOP</a></header>
    <div class="div1"><span class="ha">LABORATORY<br>IV</span></div>
    <nav>
        <ul>
            <li><a href="/">მთავარი</a></li>
            <li><a href="/about">ჩვენს შესახებ</a></li>
            <li><a href="/categories">კატეგორიები</a></li>
            <li><a href="/articles">სტატიები</a></li>
            <li><a href="/contact">კონტაქტი</a></li>
        </ul>
    </nav>
    <br>
    <form action="contact" method="POST">
        @csrf
        <h5>(*) - required</h5>
        <input type="text" name="firstname" class="inp" placeholder="Please enter your name... (*)" required>
        <br><br>
        <input type="email" name="email" class="inp" placeholder="Please enter your email address... (*)" required>
        <br><br>
        <input type="number" name="mobile" class="inp" placeholder="Please enter your mobile number...">
        <br><br>
        <textarea name="comment" cols="60" rows="10" class="inp" placeholder="Please write your comment here... (*)" required></textarea>
        <br><br><br>
        <button class="btn1">გაგზავნა</button>
    </form>
    <br>
    <hr>
    <h3>დაგვიკავშირდით</h3>
    <h3>საბანკო რეკივიზტები</h3>
    <p>მიმღები ბანკი: სს თიბისი ბანკი</p>
    <p>ბანკის კოდი: TBCBGE22</p>
    <p>ანგ ნომერი: GE31TB7448536080100003</p>
    <p>მიმღები: შპს WEBSITE</p>
    <p>ს/კ 400172219</p>
    <p>ლოკაცია: თბილისი, საქართველო</p>
    <p>ტელეფონი: (+995) 555 68 69 96</p>
    <p>მეილი: info@website.ge</p>
</body>
</html>
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/categories', function () {
    return view('categories');
});

Route::get('/categories1', function () {
    return view('categories1');
});

Route::get('/categories2', function () {
    return view('categories2');
});

Route::get('/categories3', function () {
    return view('categories3');
});

Route::get('/categories4', function () {
    return view('categories4');
});

Route::get('/bought', function () {
    return view('bought');
});

Route::get('/articles', function () {
    return view('articles');
});

Route::get('/article1', function () {
    return view('article1');
});

Route::get('/article2', function () {
    return view('article2');
});

Route::get('/article3', function () {
    return view('article3');
});

Route::get('/contact', function () {
    return view('contact');
});

// Route::get('/contact', function () {
//     return view('contact');
// });

Route::view('contact', 'contact');

Route::post('/contact', [ContactController::class, 'addData']);


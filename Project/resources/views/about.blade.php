<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="alternate" href="atom.xml" type="application/atom+xml" title="Atom">
    <link rel="stylesheet" href="css/style.css">
    <title>24HOUR ─ About</title>
</head>
<body>
    <div class="container">
        <div class="topbar">
            <a href="/dashboard" class="topbarlink">ACCOUNT<svg class="svg1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
            </svg></a> 
        </div>
    <header>
        <a href="/"><img src="imgs/logo.png" alt="" class="logo"></a>
    </header>
    <br><br>
    <nav>
        <ul>
            <li><a href="/about" class="navlinks">ABOUT</a></li>
            <li><a href="/faq" class="navlinks">FAQ</a></li>
            <li><a href="/memberships" class="navlinks" id="memberships">MEMBERSHIPS</a></li>
            <li><a href="/facilities" class="navlinks">FACILITIES</a></li>
            <li><a href="/contact" class="navlinks">CONTACT US</a></li>
        </ul>
    </nav>
    </div>
    <div class="div1">
        <a href="#socials" class="div1link">MAKE SURE TO FOLLOW US ON OUR SOCIALS</a>
    </div>
    <div class="left">
        <h1 class="thick">ABOUT US</h1>
        <div class="pagelinks">
            <a href="/" class="link1">HOME</a>
            <span class="slash">/</span>
            <a href="/about" class="link2">ABOUT</a>
        </div>
        <p class="abouttext">
            <p style="font-weight: bold;">‘24HOUR’</p>
            ‘24HOUR’ is your new home in boutique wellness. We join the dependable standards of Pilates with forefront development to give you<br> 
            the most out of each exercise. Our specialists are very prepared and fit the bill to furnish you with shape-centered alterations that will<br> 
            push you as far as possible without bargaining your security. Expect high-vitality quick-paced exercises that leave each muscle shaking.<br> 
            ‘24HOUR’ was established in 2012 by Lynette Banks and now we have 2 GEORGIA areas… with a third in transit! The ‘24HOUR’ Former was<br> 
            structured solely for ‘24HOUR’ to consolidate two of the best bits of Pilates hardware, the reformer, and the Pilates seat. Co-structured<br>
            with the Villency Design Group, the ‘24HOUR’ Former displays an extraordinary crossbreed of seat and reformer Pilates techniques, mixing<br> 
            quality preparing and cardio for a full-body exercise. You can anticipate brisk, consistent changes, a quiet carriage, and additional choices<br> 
            for shifting dimensions of protection from making the moves all the more difficult, at last focusing on the muscles all the more profoundly<br> 
            for a more prominent consume. Our ‘24HOUR’ IR class is warmed by cutting-edge full-range infrared boards. Full range infrared warmth is<br> 
            the most vitality productive approach to warm a space and you get a noteworthy increase in advantages just from going into the room.<br>
            You’ll get a vibe decent surge of serotonin and dopamine simply like from the sun’s characteristic beams in addition to IR has been<br> 
            appeared to help with cell fix, flow, detoxification, and stress decrease. IR warm is the following stage in exercise advancement.
        </p>
    </div>
    <br>
    <div class="div4">
        <h3><h3>KEEP AN EYE ON EMAIL UPDATES AND GET A CHANCE TO WIN 50% OFF* A CERTAIN MEMBERSHIP.</h3></h3>
    </div>
    <footer>
        <div class="customerservice">
            <p class="title2">CUSTOMER SERVICE</p>
            <p><a href="/account/registration" class="footerlinks">Register a New Account</a><p>
            <p><a href="/contact" class="footerlinks">Contact Us</a></p>
            <p><a href="/faq" class="footerlinks">FAQs</a></p>
        </div>
        <div class="myaccount">
            <p class="title2">MY ACCOUNT</p>
            <p><a href="/account/login" class="footerlinks">Sign In to My Account</a></p>
        </div>
        <div class="policies">
            <p class="title2">POLICIES</p>
            <p><a href="/privacy" class="footerlinks">Privacy & Cookie Policy</a></p>
            <p><a href="/terms" class="footerlinks">Terms & Conditions</a></p>
            <a href="/review" class="footerlinks">Review Guidelines</a>
        </div>
        <div class="followus">
            <p class="title2">FOLLOW US</p>
            <div class="socials" id="socials">
                <a href="http://www.facebook.com"><img src="imgs/facebook.png" alt=""></a>
                <a href="http://www.instagram.com"><img src="imgs/instagram.png" alt=""></a>
                <a href="http://www.twitter.com"><img src="imgs/twitter.png" alt="" id="twitter"></a>
            </div>
        </div>
    </footer>
    <div class="div5">
        <img src="imgs/logo.png" alt="" class="div5logo">
        <img src="imgs/links.png" alt="" class="payments">
    </div>
</body>
</html>